#!/bin/bash
# Copyright (c) 2018 Stefan Zimmermann / MIT License

CMD="/usr/bin/docker run --rm -i jbarlow83/ocrmypdf"
CMD_OPTS="-l eng+deu --rotate-pages --deskew --clean --skip-text"
BASEDIR=$(dirname $0)
PIDFILE=$BASEDIR/ocrmypdfs.pid

if [ -f $PIDFILE ]
then
	PID=`cat $PIDFILE`
	ps $PID > /dev/null
	if [ $? -eq 0 ]
	then
		exit 0
	fi
fi
	
echo $$ > $PIDFILE

INPUT_DIR=$BASEDIR/input
ARCHIVE_DIR=$BASEDIR/archive
OUTPUT_DIR=$BASEDIR/output

mkdir -p $INPUT_DIR $ARCHIVE_DIR $OUTPUT_DIR

for file in $INPUT_DIR/*.pdf
do
	if [ -f $file ]
	then
		$CMD $CMD_OPTS - - <$file >$OUTPUT_DIR/`basename $file`
		echo $?
		if [ $? -eq 0 ]
		then
			if [ -f $OUTPUT_DIR/`basename $file` ]
			then
				mv $file $ARCHIVE_DIR/`basename $file`
			fi
		fi
	fi
done

# Cleanup archive files when output file was moved/removed

for af in $ARCHIVE_DIR/*.pdf
do
	if [ ! -f $OUTPUT_DIR/`basename $af` ]
	then
		rm -f $af
	fi
done

rm -f $PIDFILE
