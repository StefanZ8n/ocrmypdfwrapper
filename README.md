# OCRmyPDFs #

# About
`ocrmypdfs.sh` is a Linux shell script with automatically transforms  normal PDF files to searchable PDF files.

# How it works
This script makes use of the Docker image `jbarlow83/ocrmypdf` to run an OCRmyPDF container and turn normal PDFs to searchable PDFs.
A new Docker container will be started for each PDF file and will be removed after the command finished.

`ocrmypdfs` reads all `*.pdf` files from an `INPUT_DIR`, saves the searchable PDFs in an `OUTPUT_DIR` and moves the original files to an `ARCHIVE_DIR`, in case that there were issues with the transformation.

To prevent the script from running twice at the same time (which would mean maybe also transform the same files at the same time), it creates a `PIDFILE` and checks for its existance first. If it's found a new call of the script will just end without any action.

At the end of the script an automated cleanup of the `ARCHIVE_DIR` happens. All the files which have been removed (renamed or deleted) from `OUTPUT_DIR` will also be removed from the `ARCHIVE_DIR`.

# Resources

- [OCRmyPDF on GitHub](https://github.com/jbarlow83/OCRmyPDF)

# Info
Written by Stefan Zimmermann <stefan@z8n.eu>